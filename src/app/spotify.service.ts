import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SpotifyService {

  constructor(private http: Http) { }

  search(query: string, limit = 12) {
    return this.http.get('https://api.spotify.com/v1/search?q=' + query + '&limit=' + limit + '&type=track,album,artist');
  }

  getAlbum(id: string) {
    return this.http.get('https://api.spotify.com/v1/albums/' + id);
  }

  getArtist(id: string) {
    return this.http.get('https://api.spotify.com/v1/artists/' + id);
  }

  getArtistAlbum(id: string, limit = 21) {
    return this.http.get('https://api.spotify.com/v1/artists/' + id + '/albums?limit=' + limit);
  }

}
