import { Component } from '@angular/core';

@Component({
  selector: 'ngSpt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngSpt works!';
}
