import {Routes, RouterModule} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {IndexComponent} from './index/index.component';
import {OneAlbumComponent} from './one-album/one-album.component';
import {OneArtistComponent} from './one-artist/one-artist.component';
import {AboutComponent} from './about/about.component';

const APP_ROUTE: Routes = [
  {path: '', component: IndexComponent},
  {path: 'album/:id', component: OneAlbumComponent},
  {path: 'artist/:id', component: OneArtistComponent},
  {path: 'about', component: AboutComponent}
];

export const routing = RouterModule.forRoot(APP_ROUTE);
