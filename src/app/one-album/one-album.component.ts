import {Component, OnInit, OnDestroy} from '@angular/core';
import {SpotifyService} from '../spotify.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngSpt-one-album',
  templateUrl: './one-album.component.html',
  styleUrls: ['./one-album.component.css']
})
export class OneAlbumComponent implements OnInit, OnDestroy {
  currentSongUrl: string = '';
  album: any;
  audio = new Audio();

  constructor(private spotifyService: SpotifyService, private route: ActivatedRoute) { }

  ngOnDestroy() {
    this.audio.pause();
  }
  ngOnInit() {
    this.route.params
      .subscribe((params) => {
        this.spotifyService.getAlbum(params['id']).subscribe((data) => {
          this.album = data.json();
        });
      });
  }

  openSong(trackUrl: string) {
    if (trackUrl === this.currentSongUrl && !this.audio.paused) {
      this.audio.pause();
    }else if (trackUrl === this.currentSongUrl && this.audio.paused) {
      this.audio.play();
    }else {
      this.audio.src = trackUrl;
      this.audio.load();
      this.audio.play();
      this.currentSongUrl = trackUrl;
    }
  }
}
