import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IndexComponent } from './index/index.component';
import { routing } from './ng-spt.route';
import { SpotifyService } from './spotify.service';
import {AlbumComponent} from './index/album/album.component';
import {ArtistComponent} from './index/artist/artist.component';
import {TrackComponent} from './index/track/track.component';
import { OneAlbumComponent } from './one-album/one-album.component';
import { OneArtistComponent } from './one-artist/one-artist.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IndexComponent,
    AlbumComponent,
    ArtistComponent,
    TrackComponent,
    OneAlbumComponent,
    OneArtistComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
