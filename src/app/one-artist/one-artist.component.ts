import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from '../spotify.service';

@Component({
  selector: 'ngSpt-one-artist',
  templateUrl: './one-artist.component.html',
  styleUrls: ['./one-artist.component.css']
})
export class OneArtistComponent implements OnInit {
  artist: any;
  albums: any;
  constructor(private spotifyService: SpotifyService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe((params) => {
        this.spotifyService.getArtist(params['id']).subscribe((data) => {
          this.artist = data.json();
        });
        this.spotifyService.getArtistAlbum(params['id']).subscribe((data) => {
          console.log(data.json());
          this.albums = data.json();
        });
      });
  }

}
