/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { OneArtistComponent } from './one-artist.component';

describe('OneArtistComponent', () => {
  let component: OneArtistComponent;
  let fixture: ComponentFixture<OneArtistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneArtistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneArtistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
