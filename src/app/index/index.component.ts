import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {SpotifyService} from '../spotify.service';

@Component({
  selector: 'ngSpt-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {
  @Input() trackURL: string;
  searchQuery: string = '';
  lookingQuery: string = '';
  results: any;
  currentSongUrl: string = '';
  audio = new Audio();

  constructor(private spotifyServise: SpotifyService) { }

  ngOnInit() {
  }
  search(query: string) {
    if (query.length && (this.lookingQuery !== query)) {
      this.lookingQuery = query;
      this.spotifyServise.search(this.lookingQuery).subscribe(
        (data) => {
          this.results = data.json();
        }
      );
    }
  }

  ngOnDestroy() {
    this.audio.pause();
  }

  openSong(trackUrl) {
    if (trackUrl === this.currentSongUrl && !this.audio.paused) {
      this.audio.pause();
    }else if (trackUrl === this.currentSongUrl && this.audio.paused) {
      this.audio.play();
    }else {
      this.audio.src = trackUrl;
      this.audio.load();
      this.audio.play();
      this.currentSongUrl = trackUrl;
    }
  }
}
