import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'ng-spt-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {
  @Input() track: any;
  @Output() trackURL= new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  openSong(songUrl: string) {
    this.trackURL.emit(songUrl);
  }

}
