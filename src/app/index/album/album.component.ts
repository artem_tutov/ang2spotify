import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-spt-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
    @Input() album: any;
  constructor() { }

  ngOnInit() {
  }

}
