import { browser, element, by } from 'protractor';

export class Ang2SpotifyPage {
  navigateTo() {
    return browser.get('/');
  }

  getSearchInput() {
    return element(by.css('ngSpt-root input'));
  }
}
