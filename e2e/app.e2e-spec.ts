import { Ang2SpotifyPage } from './app.po';

describe('ang2-spotify App', function() {
  let page: Ang2SpotifyPage;

  beforeEach(() => {
    page = new Ang2SpotifyPage();
  });

  it('should display search input', () => {
    page.navigateTo();
    expect(page.getSearchInput());
  });
});
